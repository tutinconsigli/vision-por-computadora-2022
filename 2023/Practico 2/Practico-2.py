import cv2
import numpy as np


def seleccionar_puntos(event, x, y, flags, param):		# Función para seleccionar puntos en una imagen
    global puntos
    if event == cv2.EVENT_LBUTTONDOWN:
        puntos.append([x, y])
        cv2.circle(imagen1, (x, y), 5, (0, 0, 255), -1)
        cv2.imshow('Imagen1', imagen1)

print('Seleccione tres puntos en la imagen.')
print('Una imagen nueva se incrustara en los puntos seleccionados.')
print('Luego, presione cualquier tecla para salir.')

imagen1 = cv2.imread('imagen1.jpg')   				# Cargar la primera imagen


resultado = imagen1.copy()					# Crear una copia de la imagen para incrustar la segunda imagen


cv2.namedWindow('Imagen1')					# Crear una ventana para mostrar la imagen
cv2.setMouseCallback('Imagen1', seleccionar_puntos)

puntos = []							# Lista para almacenar los puntos seleccionados

while len(puntos) < 3:						# Mostrar la imagen y esperar a que se seleccionen los puntos
    cv2.imshow('Imagen1', imagen1)
    cv2.waitKey(1)

puntos = np.float32(puntos)					# Coordenadas de los puntos seleccionados


imagen2 = cv2.imread('imagen2.jpg')				# Cargar la segunda imagen
									#cv2.namedWindow('Imagen2')     Aestas 3 linesas las pusiste solo para ver si esta cargando bien la imagen 2
									#cv2.imshow('Imagen2', imagen2)
									#cv2.waitKey(1)







esquinas = np.float32([[0, 0], [imagen2.shape[1], 0], [0, imagen2.shape[0]]])				# Obtener las esquinas de la segunda imagen


matriz_afin = cv2.getAffineTransform(esquinas, puntos)							# Calcular la matriz de transformación afín


imagen2_transformada = cv2.warpAffine(imagen2, matriz_afin, (imagen1.shape[1], imagen1.shape[0]))	# Aplicar la transformación afín a la segunda imagen


mascara = cv2.warpAffine(np.ones_like(imagen2[:, :, 0]), matriz_afin, (imagen1.shape[1], imagen1.shape[0])).astype(np.uint8) * 255  #ESTA ES LA LINEA DONDE ESTABA EL ERROR, LA MASCARA ESTABA MAL GENERADA

imagen_resultado = cv2.bitwise_and(imagen1, imagen1, mask=cv2.bitwise_not(mascara))			# Aplicar la máscara a la imagen transformada
imagen_resultado = cv2.bitwise_or(imagen_resultado, imagen2_transformada)				











cv2.imshow('Resultado', imagen_resultado)								# Mostrar el resultado
cv2.waitKey(0)
cv2.destroyAllWindows()
























