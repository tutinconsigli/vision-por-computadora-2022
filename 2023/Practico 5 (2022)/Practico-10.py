#Práctico 10 - Práctico Libre ArUCo
#El trabajo puede ser, por ejemplo, referido a realidad virtual en 2D o en 3D, estimación de posición y orientación de objetos en movimiento, seguimiento de objetos, etc. También puede tomarse como base algún repositorio interesante, instalarlo, hacerlo funcionar y aplicarlo para un uso personal o particular.

#El codigo utilizado es propiedad de Pysource, en la pagina del usuario, ahi hay una breve explicacion del codigo y un video mostrando la escritura del mismo paso a paso
#https://pysource.com/2021/05/28/measure-size-of-an-object-with-opencv-aruco-marker-and-python/

#El programa sirve para medir objetos (con un marcador aruco como referencia) utilizando una camara. 
#A esto se le puede dar multiples aplicaciones, un ejemplo de ello puede ser utilizarlo para clasificar piezas en un proceso de produccion, para luego separarlas por tamaño.

#Tanbien quise probar con un programa que media distancia hasta un marcador, pero estaba programado para ser usado con una raspberry y no logre hacerlo funcionar con la camara de la computadora (Dejo el link por si es de interes #https://www.youtube.com/watch?v=xzG2jQfxLlY)

import cv2
from object_detector import *
import numpy as np

# Load Aruco detector
parameters = cv2.aruco.DetectorParameters_create()
aruco_dict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_5X5_50)


# Load Object Detector
detector = HomogeneousBgDetector()

# Load Cap
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

while True:
    _, img = cap.read()

    # Get Aruco marker
    corners, _, _ = cv2.aruco.detectMarkers(img, aruco_dict, parameters=parameters)
    if corners:

        # Draw polygon around the marker
        int_corners = np.int0(corners)
        cv2.polylines(img, int_corners, True, (0, 255, 0), 5)

        # Aruco Perimeter
        aruco_perimeter = cv2.arcLength(corners[0], True)

        # Pixel to cm ratio
        pixel_cm_ratio = aruco_perimeter / 20

        contours = detector.detect_objects(img)

        # Draw objects boundaries
        for cnt in contours:
            # Get rect
            rect = cv2.minAreaRect(cnt)
            (x, y), (w, h), angle = rect

            # Get Width and Height of the Objects by applying the Ratio pixel to cm
            object_width = w / pixel_cm_ratio
            object_height = h / pixel_cm_ratio

            # Display rectangle
            box = cv2.boxPoints(rect)
            box = np.int0(box)

            cv2.circle(img, (int(x), int(y)), 5, (0, 0, 255), -1)
            cv2.polylines(img, [box], True, (255, 0, 0), 2)
            cv2.putText(img, "Width {} cm".format(round(object_width, 1)), (int(x - 100), int(y - 20)), cv2.FONT_HERSHEY_PLAIN, 2, (100, 200, 0), 2)
            cv2.putText(img, "Height {} cm".format(round(object_height, 1)), (int(x - 100), int(y + 15)), cv2.FONT_HERSHEY_PLAIN, 2, (100, 200, 0), 2)



    cv2.imshow("Image", img)
    key = cv2.waitKey(1)
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()

