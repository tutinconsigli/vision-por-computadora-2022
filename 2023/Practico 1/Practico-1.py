 #Empieza a explicar en 1:20:40 clase grabada 3 mas en 1:21:00, hasta 1:27:35
# A. Usando como base el programa anterior, escribir un programa que permita seleccionar
#una porción rectangular de una imagen, luego:
#Con la letra “g” guardar la porción de la imagen seleccionada como una nueva imagen,
#Con la letra “r” restaurar la imagen original y permitir realizar una nueva selección,
#Con la “q” finalizar.
 
 
import cv2
import numpy as np
blue = ( 255 , 0 , 0 ) ; green = ( 0 , 255 , 0 ) ; red = ( 0 , 0 , 255)
drawing = False                                                                                      # true si el botón está presionado
mode = True                                                                                          # si True, rectángulo, sino línea, cambia con ’m’
xybutton_down = -1 , -1                                                                              
xybutton_up = -1 , -1                                                                               #guardas la pos donde soltaste el click, para recortar la imagen

print('Seleccione una porcion de la imagen, luego recortela con la letra g.')
def recorta ( event , x , y , flags , param ) :    

    global xybutton_down, xybutton_up , drawing , mode
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        xybutton_down = x , y
    elif event == cv2 .EVENT_MOUSEMOVE:                                             #evento: se mueve el mouse
        if drawing is True :                                                        #esta dibujando?                                         
                img [ : ] = img2
                if mode is True:
                    cv2.rectangle( img , xybutton_down , ( x , y ) , blue , 2)     
    elif event == cv2.EVENT_LBUTTONUP:                                              #si soltas el click
        drawing = False                                                             #modo pasa a falso, deja de dibujar
        xybutton_up = x , y                                                         #carga la pos del click en xybutton_up


img = cv2.imread ('hoja.png',1)                                                                     #lee la imagen
img2 = cv2.imread ('hoja.png',1)                                                                    #GUARDASTE UNA SEGUNDA IMAGEN PARA PODER ACTUALIZAR

cv2.namedWindow( ' image ' )
cv2.setMouseCallback ( ' image ' , recorta )                                                        #cuando pasa algo con un click, llama a recorta


while ( 1 ) :
    cv2.imshow ( ' image ' , img )
    k = cv2.waitKey ( 1 ) & 0xFF
    if k == 113 :                                                                                   #Q en ascii es 113, g = 103
        break 
    elif k == 114 :                                                                                 #r en ascii es 114 con esto carga la imagen de vuelta
        img [ : ] = img2  
    elif k == 103 :                                                                                  #g = 103
        x1, y1 = xybutton_down
        x2, y2 = xybutton_up
        img_nueva = img[y1:y2, x1:x2]                                                                                 
        cv2.imwrite ('Imagennueva.jpg', img_nueva)
        cv2.imshow ('Imagennueva.jpg', img_nueva)
cv2.destroyAllWindows ( )

