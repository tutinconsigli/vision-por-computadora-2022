import cv2 as cv
import numpy as np
import math

blue = (255, 0, 0); green = (0, 255, 0); red = (0, 0, 255);
drawing = False
Coord = 0, 0
Coordenada1 = 0, 0
Coordenada2 = 0, 0
Coordenada3 = 0, 0
Coordenada4 = 0, 0
aux = 0
    
def mouse (evento, x, y, flags, param ) :                                                 #Define la funcion mouse
    global Coordenada1, Coordenada2, Coordenada3, Coordenada4, drawing, img_respaldo, aux #dice que variables globales usa
    if evento == cv.EVENT_LBUTTONDOWN:                                                    #if apretamos el click izq
        drawing = True
        Coord = x, y                                                                      #en la variable coord va a ir guardando la pos del mouse
        cv.circle(img_respaldo, Coord, radius=2, color=(0, 0, 255), thickness=2)          #dibuja un punto rojo donde haga click
        if aux == 0:                                                                      #se fija, con la variable aux, que numero de "click" es
            Coordenada1 = Coord                                                           #guarda pos en coord1
            aux += 1
        elif aux == 1:
            Coordenada2 = Coord
            aux += 1
        elif aux == 2:
            Coordenada3 = Coord
            aux += 1
        elif aux == 3:
            Coordenada4 = Coord

def homografia (punto_1, punto_2, imagen):
    al, an = imagen.shape[:2]                                                             #guarda el tamaño de la imagen
    Matriz = cv.getPerspectiveTransform(punto_1, punto_2)                                 #Calcula una transformación de perspectiva a partir de cuatro pares de los puntos corresp.
    img_salida = cv.warpPerspective(imagen, Matriz, (al, an))                             #La función warpPerspective() devuelve una imagen o video cuyo tamaño es el mismo que el 
    return img_salida                                                                     #tamaño de la imagen o video original.

img_original = cv.imread ('large_thumbnail.jpg', 1)                                       #guarda la imagen
img = cv.resize(img_original, (500, 500))                                                 #le cambia el tamaño a la imagen
img_respaldo = img.copy()                                                                 #cambia el tamaño de la imagen
img_homografica = img.copy()                                                              
cv.namedWindow ('Imagen original.')
cv.setMouseCallback ('Imagen original.', mouse)
while (1) :
    cv.imshow ('Imagen original.', img_respaldo)                                          #muestra la imagen 
    k = cv.waitKey (1) & 0xFF                                                             #espera
    if k == ord("r"):
        img_respaldo = img.copy()                                                         #con r limpiamos los puntos de la imagen
    elif k == ord("h"):
        al, an = (300, 300)                                                               #eligimos el tamaño de la imagen de salida  
        punto_1 = np.float32([[Coordenada1], [Coordenada2], [Coordenada3], [Coordenada4]])
        punto_2 = np.float32([[0, 0], [an, 0], [0, al], [an, al]])
        img_salida = homografia(punto_1, punto_2, img)
        img_salida = img_salida[0:al, 0:an]
        cv.imshow('Imagen homografia', img_salida)                                    #muestra la imagen nueva
        cv.imwrite('Imagen final.png', img_salida)                                  #guarda la imagen
        Coordenada1 = Coordenada2 = Coordenada3 = Coordenada4 = 0, 0                      #devuelve coordenadas a cero
        aux = 0                                                                           #pone auxa a cero para poder empezar a contar los puntos de vuelta
    elif k == ord('q'):
        break
cv.destroyAllWindows()


