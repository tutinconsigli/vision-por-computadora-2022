# A - ¿Cómo obtener el frame rate o fps usando las OpenCV? Usarlo para no tener que harcodear el delay del waitKey
 
# B - ¿Cómo obtener el ancho y alto de las imágenes capturadas usando las OpenCV? Usarlo para no tener que harcodear el frameSize del video generado.


import sys                                                          #Explicado en clase grabada 2- 1:50:00
import cv2

if(len(sys.argv) > 1) :                                             #mido largo, si es mayor que 1, traigo el archivo
    filename = sys.argv [1]                                         #guarda en filename, (primer argumento es el nombre del archivo, el segundo es el video)
else:
    print('Pass a filename as first argument.')                      #si no, te imprime un error y te pide que le psaes algo
    sys.exit(0)

cap = cv2 . VideoCapture(filename)                                  #si el filename esta bien, con video capture abrimos el video

fourcc = cv2.VideoWriter_fourcc ( 'X','V','I','D' )

#framesize = (640, 480)                                             #Esta linea es la que queremos evitar en el punto B

alto=int(cap.get(3))                                                #cap.get(3) usa CAP_PROP_FRAME_WIDTH, toma el ancho de la imagen, (lo pusiste al fondo)
ancho=int(cap.get(4))                                               #cap.get(4) CAP_PROP_FRAME_HEIGHT, toma el alto de la imgen
framesize=(alto,ancho)                                              #designa framesize
print('Framesize: {}'.format(framesize))


out = cv2.VideoWriter ( ' output . avi ' , fourcc , 20.0 , framesize )

#delay = 33                                                         #Esta linea es la que queremos evitar en el punto A
 
fps = cap.get(cv2.CAP_PROP_FPS)                                     #con cap.get(cv2.CAP_PROP_FPS) tomamos las fps (tambien podriamos haber puesto un 5)
print ("FPS: {}".format(fps))
delay=int(round((1/round(fps))*1000)-1)                             #transformo a milisegundos
print("Delay: {}".format(delay))




while (cap.isOpened()) :                                            #Con el while capturamos frame a frame
    ret , frame = cap.read()
    if ret is True :
        gray = cv2.cvtColor (frame, cv2 .COLOR_BGR2GRAY)            #convertimos a escala de grises
        out.write (gray)
        cv2.imshow ('Image gray' ,gray)                             #mostramos la imagen
        if cv2.waitKey (delay) & 0xFF == ord ('q') :                #si antes de que se termine el archivo apretamos "q", sale
            break
        else:
            break

cap.release()                                                                      
out.release()
cv2.destroyAllWindows()
                                                                    #Los videos tienen una tasa de captura por segundo FPS(frames per second)(cantidad de imagenes por segundo)
                                                                    #una camara normal captura mas o menos a 30 FPS, si nosotros queremos levantar ese video tenemos que dividir 
                                                                    #un segundo por la cantidad de imagenes que saca la camara(por seguno)
                                                                    #en este caso que son 30 FPS, nos queda 33 milisegundos, que es lo que pone en la linea 23 (declarado en la 
                                                                    #linea 16) dentro de la funcion cv2.waitkey (delay=33)
                                                                    #cada 33 milisegundos muestra una nueva imagen
                                                                    #cambiando el waitkey el video se ve mas acelerado o mas lento
                                                                    
                                                                    


#Info util:
#Para la funcion cap.get(), el numero que le pones desntro de los parentesis, cumple una de las funciones de abajo
# a estso lo sacaste de 
#https://stackoverflow.com/questions/52823439/what-is-the-role-of-get-and-how-to-adjust-frame-rate
#https://docs.opencv.org/3.1.0/d8/dfe/classcv_1_1VideoCapture.html#aeb1644641842e6b104f244f049648f94

#en este tp usas solo 3, 4 y 5

#0 CAP_PROP_POS_MSEC Current position of the video file in milliseconds or video capture timestamp.
#1 CAP_PROP_POS_FRAMES 0-based index of the frame to be decoded/captured next.
#2 CAP_PROP_POS_AVI_RATIO Relative position of the video file: 0 - start of the film, 1 - end of the film.
#3 CAP_PROP_FRAME_WIDTH Width of the frames in the video stream.
#4 CAP_PROP_FRAME_HEIGHT Height of the frames in the video stream.
#5 CAP_PROP_FPS Frame rate.
#6 CAP_PROP_FOURCC 4-character code of codec.
#7 CAP_PROP_FRAME_COUNT Number of frames in the video file.
#CAP_PROP_FORMAT Format of the Mat objects returned by retrieve() .
#CAP_PROP_MODE Backend-specific value indicating the current capture mode.
#CAP_PROP_BRIGHTNESS Brightness of the image (only for cameras).
#CAP_PROP_CONTRAST Contrast of the image (only for cameras).
#CAP_PROP_SATURATION Saturation of the image (only for cameras).
#CAP_PROP_HUE Hue of the image (only for cameras).
#CAP_PROP_GAIN Gain of the image (only for cameras).
#CAP_PROP_EXPOSURE Exposure (only for cameras).
#CAP_PROP_CONVERT_RGB Boolean flags indicating whether images should be converted to RGB.
#CAP_PROP_WHITE_BALANCE Currently not supported
#CAP_PROP_RECTIFICATION Rectification flag for stereo cameras (note: only supported by DC1394 v 2.x backend currently)
                                               
                                                            
