#Explica en clase grabada 6
# A. Crear una función que aplique una transformación euclidiana, recibiendo los siguientes parámetros:
#angle: Ángulo
#tx: traslación en x
#ty: traslación en y
#B. Usando como base el programa anterior, escribir un programa que permita seleccionar
#una porción rectangular de una imagen y
#Con la letra “e” aplique una transformación euclidiana a la porción de imagen
#seleccionada y la guarde como una nueva imagen.
 
 
import cv2
import numpy as np
blue = ( 255 , 0 , 0 ) ; green = ( 0 , 255 , 0 ) ; red = ( 0 , 0 , 255)
drawing = False                                                                     # true si el botón está presionado
mode = True                                                                         
xybutton_down = -1 , -1                                                                          
xybutton_up = -1 , -1                                                               #guardas la pos donde soltaste el click, para recortar la imagen

def recorta ( event , x , y , flags , param ) :    

    global xybutton_down, xybutton_up , drawing , mode
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        xybutton_down = x , y
    elif event == cv2 .EVENT_MOUSEMOVE:                                             #evento: se mueve el mouse
        if drawing is True :                                                        #esta dibujando?                                         
                img [ : ] = img2
                if mode is True:
                    cv2.rectangle( img , xybutton_down , ( x , y ) , blue , 2)     
    elif event == cv2.EVENT_LBUTTONUP:                                              #si soltas el click
        drawing = False                                                             #modo pasa a falso, deja de dibujar
        xybutton_up = x , y                                                         #carga la pos del click en xybutton_up


def rotate (image, angle, center=None , scale = 1.0):
    (h,w) = image.shape [:2]    
    if center is None :
        center = (w/2 , h/2)
    M = cv2.getRotationMatrix2D (center, angle, scale)
    rotated = cv2.warpAffine (image, M, (w,h))
    return rotated  
    
    
def translate (image, x, y) :
    (h,w) = (image.shape[0], image.shape[1])                                        #obtengo alto y ancho de la imagen
    M=np.float32 ([[1,0,x],
                   [0,1,y]])
    shifted=cv2.warpAffine(image,M,(w,h))
    return shifted


img = cv2.imread ('hoja.png',1)                                                     #lee la imagen
img2 = cv2.imread ('hoja.png',1)                                                    #guardaste una segunda imagen x si la necesitas mas adelante
cv2.namedWindow( ' image ' )
cv2.setMouseCallback ( ' image ' , recorta )                                        #cuando pasa algo con un click, llama a recorta

print('Seleccione una porcion de la imagen, luego recortela con la letra g.')
print('Cuando aparezca la nueva imagen, puede rotarla y trasladarla apretando la letra e')


while ( 1 ) :
    cv2.imshow ( ' image ' , img )
    k = cv2.waitKey ( 1 ) & 0xFF
    
    if k == 114 :                                                                #r en ascii es 114 con esto carga la imagen de vuelta
        img [ : ] = img2  
    elif k == 103 :                                                                #g = 103
        x1, y1 = xybutton_down
        x2, y2 = xybutton_up
        img_nueva = img[y1:y2, x1:x2]                                                                                 
        
        cv2.imshow ('Imagen recortada', img_nueva)
    
    elif  k == ord("e"):		
        print('Ingrese el angulo')
        angle = int(input())
        print('Ingrese traslación en x')
        x = input()
        print('Ingrese traslación en y')
        y = input()
        img_trasladada = translate(img_nueva, x, y)
        center=None
        img_rotada = rotate (img_trasladada, angle, center)

        cv2.imshow ('Imagen rotada y trasladada', img_rotada)
#       cv2.imwrite('Imagen rotada y trasladada.jpg', img_rotada)        
        print('Ingresar la letra q para finalizar')

    if k == 113 :                                                                  #Q en ascii es 113, g = 103
            break         
        
    
cv2.destroyAllWindows ( )













